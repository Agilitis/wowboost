'use strict';
const handlePostBack = require('./handlePostBack'),
    handleQuickReply = require('./handleQuicReply'),
    handleTxtMessage = require('./handleTxtMessage'),
    handleAttachments = require('./handleAttachments'),
    handleUserState = require('./handleUserState'),
    chainOfPromises = require('chain-of-promises'),
    userStates = require('../model/userstates'),
    bot_text = require('../static/bot_text.json'),
    keywords = require('../static/keywords.json'),
    _ = require('underscore'),
    users = require('../model/users'),
    MessengerPlatform = require("facebook-bot-messenger");


let catchKeyword = function (data) {

    let text = data.payload.message.text;
    return new Promise((resolve, reject) => {
        reject();
        keywords.forEach(keyword => {
            keyword.words.forEach(word => {
                if (text.toLowerCase() === word) {
                    let random = _.random(0, keyword.responses[data.user.preferredLanguage].length-1);
                    resolve({text: keyword.responses[data.user.preferredLanguage][random]})
                }
            });
        });
        reject()
    })
};

let catchProxy = function (data) {
    return new Promise((resolve, reject) => {
        if (data.payload.message && data.payload.message.quick_reply) {
            resolve(handleQuickReply(data))
        }
        reject()
    })
};

let catchQuickReply = function (data) {
    return new Promise((resolve, reject) => {
        if (data.payload.message && data.payload.message.quick_reply) {
            resolve(handleQuickReply(data))
        }
        reject()
    })

};

let catchPostback = function (data) {
    return new Promise((resolve, reject) => {
        if (data.payload.postback && data.payload.postback.payload) {
            resolve(handlePostBack(data))
        }
        reject()
    })

};

let catchText = function (data) {
    return new Promise((resolve, reject) => {
        if (data.payload.message && data.payload.message.text) {
            resolve(handleTxtMessage(data))
        }
        reject()
    })
};

let catchAttachment = function (data) {
    return new Promise((resolve, reject) => {
        if (data.payload.message.attachments) {
            resolve(handleAttachments(data));
        }
        reject()
    });
};
let defaultMessage = function (data) {
    return new Promise((resolve, reject) => {
        let preferredLanguage = data.user.preferredLanguage || 'hun';
        let builder = new MessengerPlatform.QuickRepliesMessageBuilder(bot_text.text.default_message[preferredLanguage]);
        users.setState(data.user._id, userStates.locationInput);
        builder.addLocationOption();
        resolve(builder.buildMessage());
    });
};

function pipeline(data) {
    return chainOfPromises([
        (data) => catchKeyword(data),
        (data) => catchProxy(data),
        (data) => catchQuickReply(data),
        (data) => catchPostback(data),
        (data) => catchAttachment(data),
        (data) => catchText(data),
        (data) => defaultMessage(data)
    ], data);
}


module.exports = {
    handleRequest: pipeline
};