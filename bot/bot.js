'use strict';
const Bot = require('messenger-bot'),
    botPipeLine = require('./pipeline'),
    MessengerPlatform = require('facebook-bot-messenger'),
    bot_text = require('../static/bot_text.json'),
    users = require('../model/users'),
    logger = require('./Logger');

process.env.NODE_CONFIG_DIR = '../config';
const config = require('config');
let bot = new Bot({
    token: config.get('pageAccessToken')
});


function getUserAndHandleRequest(payload, reply) {
    let userId = payload.sender.id;
    users.findUser(userId)
        .then(user => {
            if (!user) {
                //INIT USER
                bot.getProfile(userId, (err, profile) => {
                    if (err) {
                        reply({text: "sajnos valami hiba történt"})
                    } else {
                        profile._id = userId;
                        profile.state = "default";
                        profile.preferredLanguage = 'hun';
                        console.log("INIT USER: ", profile);
                        users.insertUser(profile);
                        botPipeLine.handleRequest({payload: payload, user: profile})
                            .then(msgToSend => {
                                if (msgToSend instanceof Error) {
                                    logger.logError("bot.js:  ", msgToSend, "msgToSend errorként jött visszaa a hanldeRequestből");
                                    reply(bot_text.text.default_error[profile.preferredLanguage])
                                }
                                else {
                                    reply(msgToSend, logger.replyErrorCallback)
                                }
                            })
                            .catch(err => {
                                reply(bot_text.text.default_error[profile.preferredLanguage]);
                                logger.logError("bot.js: ",err, "getProfile error")
                            })
                    }
                });
            }
            else {
                //TODO logging
                botPipeLine.handleRequest({payload: payload, user: user})
                    .then(msgToSend => {
                        if (msgToSend instanceof Error) {
                            console.log("HIBA: ", msgToSend);
                        }
                        else {
                            reply(msgToSend, logger.replyErrorCallback)
                        }
                    })
                    .catch(err => {
                        logger.logError("bot[sendMessage]: ",err, "userID :" + user._id);
                    })
            }
        })
        .catch((err) => {
            console.log("hiba" + err);
            reply(bot_text.text.default_error[user.preferredLanguage]);
        });

}

bot.on('error', (err) => {
    //TODO logging
    console.log(err.message)
});
bot.on(MessengerPlatform.Events.MESSAGE, (payload, reply) => {
    // actions.setTyping(true);
    getUserAndHandleRequest(payload, reply);
});

bot.on(MessengerPlatform.Events.POSTBACK, (payload, reply) => {
    // actions.setTyping(true);
    getUserAndHandleRequest(payload, reply);
});


module.exports = bot;
