'use strict';
const rp = require('request-promise');

module.exports = {
    replyErrorCallback: (err, info) => {
        if(err){
            console.log(err);
        }
    },
    logError: (source ,err, inf = null) =>{
        let options = {
            method: 'POST',
            url: 'https://hooks.slack.com/services/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',
            formData:
            { payload: '{"text": "'+ source + (err.message +"\n" + inf)+'"}' }
        };
        rp(options)
            .then(res=>{
                console.log(res);

            })
    },
    logInfo: (source, inf = null) =>{
    let options = {
        method: 'POST',
        url: 'https://hooks.slack.com/services/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',
        formData:
        { payload: '{"text": "'+ source + inf+'"}' }
    };
    rp(options)
        .then(res=>{
            console.log(res);

        })
}
};