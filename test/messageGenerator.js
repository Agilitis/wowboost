/**
 * Created by Agilitis on 27/07/2017.
 */
'use strict';
const _ = require('underscore');

class IncomingMessage {
    constructor(pageId, userId) {
        this.pageId = pageId;
        this.userId = userId;
        this.object = "page";
        this.rootObject = {
            "object": this.object,
            "entry": [
                {
                    "id": this.pageId,
                    "time": Date.now(),
                    "messaging": [
                        {
                            "sender": {"id": this.userId},
                            "recipient": {"id": this.pageId},
                            "timestamp": Date.now()
                        }
                    ]
                }
            ]
        };
    }

    locationMessage(lat, long) {
        let rootObject = _.extend(this.rootObject);
        rootObject.entry[0].messaging[0].message = _.clone({
            "mid": "mid.$cAAZWLiaTZ1NjlW0JG1dZCe9nuMxe",
            "seq": 934896,
            "attachments": [
                {
                    "title": "Users's Location",
                    "url": "https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.bing.com%2Fmaps%2Fdefault.aspx%3Fv%3D2%26pc%3DFACEBK%26mid%3D8100%26where1%3D47.5137168%252C%2B19.1729604%26FORM%3DFBKPL1%26mkt%3Den-US&h=ATNj2X7xMGOyQVHzLRV5zsxycdlaVEKVbGPp5XCbWj9A7hi75pZ622_bF9JwKHE4_XZyxbMv4jfpcdkkuuAW9jgKn6HMDgaHK_SSvXBZef5jHGmNFIM&s=1&enc=AZNtFynxPwJ-Lvc2lbjXBdSDLB7WqLeMXRmdd_IscrMi9GrWWl1u9nzyjs_0sNLzU12O4UM2Iu9v2PE1a_rNcD_n",
                    "type": "location",
                    "payload": {"coordinates": {"lat": lat, "long": long}}
                }
            ]
        });
        return rootObject;
    }

    quickReplyMessage(payload, text) {
        let rootObject = _.extend(this.rootObject);
        rootObject.entry[0].messaging[0].message = _.clone({
            "quick_reply": {"payload": payload},
            "mid": "mid.$cAAZWLiaTZ1NjlVfU-ldZBKKk42t3",
            "seq": 934644,
            "text": text
        });
        return rootObject;
    }

    postBackMessage(payload) {
        let rootObject = _.extend(this.rootObject);
        rootObject.entry[0].messaging[0].postback = {"payload": payload};
        return rootObject;
    }

    textMessage(text) {
        let rootObject={};
        _.extend(rootObject, this.rootObject);
        rootObject.entry[0].messaging[0].message = _.clone({
            "mid": "mid.$cAAZWLiaTZ1NjcvBkV1dQasVbegT_",
            "seq": 929191,
            "text": text
        });
        return rootObject;
    }

    stickerAttachment(url, sticker_id) {
        let stickerMessage = Object.assign({}, this.rootObject);
        stickerMessage.entry[0].messaging[0].message = {
            "mid": "mid.$cAAZWLiaTZ1Njltn391dZZSsbttkS",
            "seq": 935758,
            "sticker_id": sticker_id,
            "attachments": [
                {
                    "type": "image",
                    "payload": {
                        "url": url,
                        "sticker_id": sticker_id
                    }
                }
            ]
        };
        return stickerMessage;
    }
}

class OutgoingMessage {
    constructor(pageId, userId, appId) {
        this.pageId = pageId;
        this.userId = userId;
        this.appId = appId;
        this.object = "page";
    }

    textMessage(text) {
        return {
            "object": "page",
            "entry": [
                {
                    "id": this.pageId,
                    "time": Date.now(),
                    "messaging": [
                        {
                            "sender": {"id": this.pageId},
                            "recipient": {"id": this.userId},
                            "timestamp": Date.now(),
                            "message": {
                                "is_echo": true,
                                "app_id": this.appId,
                                "mid": "mid.$cAAZWLiaTZ1NjtRiiH1dg9NZzq1RR",
                                "seq": 944451,
                                "text": text
                            }
                        }
                    ]
                }
            ]
        }

    }

    galleryView(elements) {
        return {
            "object": "page",
            "entry": [
                {
                    "id": this.pageId,
                    "time": Date.now(),
                    "messaging": [
                        {
                            "sender": {"id": this.pageId},
                            "recipient": {"id": this.userId},
                            "timestamp": Date.now(),
                            "message": {
                                "is_echo": true,
                                "app_id": this.appId,
                                "mid": "mid.$cAAZWLiaTZ1NjtTlUoFdg_QLSQ7HY",
                                "seq": 944616,
                                "attachments": [elements]
                            }
                        }
                    ]
                }
            ]
        }
    }
}

module.exports = {
    IncomingMessage: IncomingMessage,
    OutgoingMessage: OutgoingMessage
};