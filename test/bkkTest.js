/**
 * Created by Agilitis on 21/06/2017.
 */
'use strict';
const tap = require('tap'),
    _ = require('underscore'),
    utilFunctions = require('../util/functions'),
    BKKRequest = require('../util/BKKRequest'),
    stops = require('../static/stopId'),
request = require('../util/BKKRequest');

// tap.test('bkk request checks', childTest=> {
//     request.getLinesAtStop("F01382").then((res)=> {
//         console.log(res);
//         childTest.end()
//     }).catch(err=> {
//         console.error("something went wrong in test->" + err)
//     });
//
//     request.getScheduleForStop("F01382")
//         .then(res=>{
//             let arrivals = _.where(res.data.entry.schedules, {routeId: "BKK_3370"});
//             console.log(new Date(arrivals[0].directions[0].stopTimes[0].arrivalTime*1000).getHours());
//
//             console.log("SCHEDULE : " + res);
//         });
//     // request.getNextArrival("F01756", "BKK_0320")
//     //     .then(res=> {
//     //         console.log("Next arrival: " + res.time)
//     //     });
//
// });

// tap.test('nearby stop names', childTest=> {
//     let nearStops = utilFunctions.getNearbyStops(stops, 47.478681, 19.071231);
//     nearStops.slice(0, 8).forEach(stop=> {
//         BKKRequest.getLinesAtStop(stop.id)
//             .then(res=> {
//                 _.map(res, (route)=>{
//                     BKKRequest.getNextArrival(stop.id, route.id)
//                         .then(arrivals=>{
//                             if(arrivals){
//                                 console.log(arrivals);
//                             }
//                         });
//
//                 });
//
//             });
//     });
//     childTest.end();
// });

tap.test('test stop', childTest=>{
   BKKRequest.getLinesAtStop("CS008519")
       .then(response=>{
           console.log(response);
           childTest.end();
       })

});

