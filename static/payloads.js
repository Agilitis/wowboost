module.exports = {
    postbacks_actions: {
        get_next_arrival:"GET_NEXT_ARRIVAL",
        add_to_favourite: "ADD_TO_FAVOURITE",
        favourites: "FAVOURITES",
        delete_favourite: "DELETE_FAVOURITE",
        get_started: "GET_STARTED",
        search: "SEARCH",
        get_stop_data: "GET_STOP_DATA",
        change_preferred_language: "CHANGE_PREFERRED_LANGUAGE",
        plan_route: "PLAN_ROUTE",
        get_to_sziget: "PLAN_ROUTE_TO_SZIGET"
    },
    quick_reply_actions: {
        get_stop_data: "GET_STOP_DATA",
        get_route_data: "GET_ROUTE_DATA",
        search: "SEARCH",
        location_input: "LOCATION_INPUT",
        get_next_arrival: "GET_NEXT_ARRIVAL",
        get_more_arrivals: "GET_MORE_ARRIVALS",
        get_next_arrival_at_stop: "GET_NEXT_ARRIVAL_AT_STOP",
        get_route_data_for_direction: "GET_ROUTE_DATA_FOR_DIRECTION",
        all_stops_for_route: "all_stops_for_route",
        english_version: "english_version",
        plan_route_later: "PLAN_ROUTE_LATER"

    }
};