'use strict';
const MongoClient = require('mongodb').MongoClient;
let db;
let collection;
const logError = (err) => {
    console.log(err)
};
module.exports = {
    initConnection: (connectionString, coll) => {
        return MongoClient.connect(connectionString)
            .then(dbInstance => {
                db = dbInstance;
                collection = coll;
            }).catch(logError)
    },
    insertDocument: (obj) => {
        return db.collection(collection)
            .insertOne(obj);
    },
    findOne: (query) => {
        return db.collection(collection)
            .findOne(query)
    },
    findOneAndUpdate: (query,obj) => {
        return db.collection(collection)
            .findOneAndUpdate(query, obj);
    },
    find: (query) => {
        return db.collection(collection)
            .find(query)
    },
    addToSet: (query, obj) => {
        return db.collection(collection)
            .findOneAndUpdate(query, {$push: obj});
    },
    closeConnection: () => {
        return db.close()
    },
    setField: (query, obj) => {
        return db.collection(collection)
            .findOneAndUpdate(query, {$set: obj});
    }
};