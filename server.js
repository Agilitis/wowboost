'use strict';
const
    bodyParser = require('body-parser'),
    config = require('config'),
    crypto = require('crypto'),
    express = require('express'),
    https = require('https'),
    documentDbManager = require('./store/documentDbManager'),
bot = require('./bot/bot');

const app = express();
const webhookJsonParser = bodyParser.json({verify: verifyRequestSignature});
process.env.NODE_CONFIG_DIR = '../config';
console.log(config.get('pageAccessToken'));

// process.env.NODE_APP_INSTANCE = process.env.DEVELOPMENT ? 'development' : '';
app.set('port', process.env.PORT || 5000);
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
app.use(express.static('public'));




const connectionString = config.get('connectionString');
documentDbManager.initConnection(connectionString, config.get('collection'))
    .then(res => {
        console.log("Connected to mongoDB" + res);
        app.listen(app.get('port'), function () {
            console.log('Node app is running on port', app.get('port'));
        });
    });


/*
 * Be sure to setup your config values before running this code. You can 
 * set them using environment variables or modifying the config file in /config.
 *
 */

// App Secret can be retrieved from the App Dashboard
const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ?
    process.env.MESSENGER_APP_SECRET :
    config.get('appSecret');

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = (process.env.MESSENGER_VALIDATION_TOKEN) ?
    (process.env.MESSENGER_VALIDATION_TOKEN) :
    config.get('validationToken');

if (!(APP_SECRET && VALIDATION_TOKEN)) {
    console.error("Missing config values");
    process.exit(1);
}

/*
 * Verify that the callback came from Facebook. Using the App Secret from 
 * the App Dashboard, we can verify the signature that is sent with each 
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
function verifyRequestSignature(req, res, buf) {
    let signature = req.headers["x-hub-signature"];

    if (!signature) {
        // For testing, let's log an error. In production, you should throw an
        // error.
        //TODO create appinsight logger
        console.log(new Error("Couldn't validate the signature."));
    } else {
        let elements = signature.split('=');
        let method = elements[0];
        let signatureHash = elements[1];

        let expectedHash = crypto.createHmac('sha1', APP_SECRET)
            .update(buf)
            .digest('hex');

        if (signatureHash != expectedHash) {
            console.log(new Error("Couldn't validate the signature."));
        }
    }
}

/*
 * Use your own validation token. Check that the token used in the Webhook
 * setup is the same token used here.
 *
 */
app.get('/webhook', (req, res) => {
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === VALIDATION_TOKEN) {
        console.log("Validating webhook");
        res.status(200).send(req.query['hub.challenge']);
    } else {
        console.log("Failed validation. Make sure the validation tokens match.");
        res.sendStatus(403);
    }
});


app.post('/webhook', webhookJsonParser, (req, res) => {
    let data = req.body;
    console.log("data: " + JSON.stringify(data));


    // Make sure this is a page subscription
    if (data.object === 'page') {
        // Iterate over each entry
        // There may be multiple if batched
        data.entry.forEach(function (pageEntry) {
            let pageID = pageEntry.id;
            let timeOfEvent = pageEntry.time;
            bot._handleMessage(data)
        });
        // Assume all went well.
        //
        // You must send back a 200, within 20 seconds, to let us know you've
        // successfully received the callback. Otherwise, the request will time out.
        res.sendStatus(200);
    }
});


// Start server
// Webhooks must be available via SSL with a certificate signed by a valid 
// certificate authority.

process.on('SIGTERM', function onSigterm() {
    console.info('Got SIGTERM. Graceful shutdown start', new Date().toISOString());
    // start graceul shutdown here
    shutdown()
});

function shutdown() {
    documentDbManager.closeConnection().then(res => {
        console.log("documentdb connection closed")
    }).catch(err => {
        console.log(new Error("Error during closing connection: " + err))
    });
    console.log("shutdown")
}

module.exports = app;