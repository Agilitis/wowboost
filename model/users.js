const docDb = require('../store/documentDbManager');

module.exports = {
    findUser: (userId) => {
        return docDb.findOne({_id: userId})
    },
    insertUser: (obj) => {
        return docDb.insertDocument(obj)
    },
    setState: (userId, state) => {
        return docDb.setField({_id: userId}, {state: state})
    },
    setField: (userId, obj) => {
        return docDb.setField({_id: userId}, obj)
    },
    addToSet: (userId, obj) => {
        return docDb.findOneAndUpdate({_id: userId}, {$push: obj})
    },
    addFavourite: (userId, obj) =>{
        return docDb.findOneAndUpdate({_id: userId}, {$push: {favourites: obj}})
    },
    removeFavourite: (userId, obj) =>{
        return docDb.findOneAndUpdate({_id: userId}, {$pull: {favourites: obj}})
    },
    setPreferredLanguage: (userId, preferredLanguage)=>{
        return docDb.setField({_id: userId}, {preferredLanguage: preferredLanguage})
    }
};
