module.exports = {
    stateField: "state",
    getStarted: "get_started",
    browse: "browsing",
    searching_bus: "search_bus",
    locationInput: "location_input",
    directions: "directions",
    origin_input: {action: "origin_input", later: 0},
    destination_input: {action: "destination_input", later: 0},
    directionsToSziget: {action: "direction_to_sziget", later: 0},
    later: {action: "later", later: 0}
};